v1.0.1
Various bug fixes and performance improvements have been made.

v1.0.0
Release of Sudoku puzzle app!  Supports solving Sudoku puzzles of varying difficulties using the customary 1-9 characters, or you can switch the display to solve using letters or even colors (colors is currently in an experimental phase, you may or may not experience issues when playing with colors).