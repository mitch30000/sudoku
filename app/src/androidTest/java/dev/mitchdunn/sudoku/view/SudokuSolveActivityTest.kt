package dev.mitchdunn.sudoku.view

import android.content.Intent
import androidx.preference.PreferenceViewHolder
import androidx.test.espresso.Espresso.onData
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.rule.ActivityTestRule
import androidx.test.uiautomator.UiDevice
import dev.mitchdunn.R
import dev.mitchdunn.sudoku.model.Board
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.startsWith
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class SudokuSolveActivityTest {

    @get:Rule
    var activityRule: ActivityTestRule<SudokuSolveActivity> = object : ActivityTestRule<SudokuSolveActivity>(
            SudokuSolveActivity::class.java) {
        override fun getActivityIntent(): Intent {
            val result = Intent(getInstrumentation().targetContext, SudokuSolveActivity::class.java)
            val board = Board("825471396" +
                    "194326578" +
                    "376985241" +
                    "519743862" +
                    "632598417" +
                    "487612935" +
                    "263159784" +
                    "948267153" +
                    "751834620")
            board.finalize(Board("825471396" +
                    "194326578" +
                    "376985241" +
                    "519743862" +
                    "632598417" +
                    "487612935" +
                    "263159784" +
                    "948267153" +
                    "751834629"))
            result.putExtra("board", board)
            return result
        }
    }

    @Before
    fun resetDisplaySettings() {
        // Put device in natural/portrait mode
        val device = UiDevice.getInstance(getInstrumentation())
        device.unfreezeRotation()
        device.setOrientationNatural()
        // Reset display settings to make sure they are 1-9 at the start of each test
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Settings")).perform(click())
        onView(withText("Display Settings")).perform(click())
        onView(withText("Reset Display")).perform(click())
        // Super hacky way to make sure we have set colors to off
        onView(withText("Enable Colors")).perform(click())
        try {
            onView(withText("Cancel")).perform(click())
            onView(withText("Enable Colors")).perform(click())
            onView(withText("Cancel")).perform(click())
        } catch (ex: NoMatchingViewException) {}
        pressBack()
        pressBack()
    }

    @Test
    fun testDisplaySettings_portrait() {
        val device = UiDevice.getInstance(getInstrumentation())
        device.unfreezeRotation()
        device.setOrientationNatural()
        // waitForIdle() doesn't always work to where trying to call the options bar
        // will crash, so instead we just use a normal Thread.sleep call to wait a second.
        Thread.sleep(1000)
        testDisplaySettings()
    }

    @Test
    fun testDisplaySettings_landscape() {
        val device = UiDevice.getInstance(getInstrumentation())
        device.unfreezeRotation()
        device.setOrientationLeft()
        // waitForIdle() doesn't always work to where trying to call the options bar
        // will crash, so instead we just use a normal Thread.sleep call to wait a second.
        Thread.sleep(1000)
        testDisplaySettings()
    }

    private fun testDisplaySettings() {
        // The first part of the button is the display character, and the second part the amount
        // of that character left, this is broken up by html <sup><small> codes on the display
        // but the html doesn't show up in the espresso view text so we merge the two parts together.
        onView(withId(R.id.button1)).check(matches(withText("10")))
        onView(withId(R.id.button2)).check(matches(withText("20")))
        onView(withId(R.id.button3)).check(matches(withText("30")))
        onView(withId(R.id.button4)).check(matches(withText("40")))
        onView(withId(R.id.button5)).check(matches(withText("50")))
        onView(withId(R.id.button6)).check(matches(withText("60")))
        onView(withId(R.id.button7)).check(matches(withText("70")))
        onView(withId(R.id.button8)).check(matches(withText("80")))
        onView(withId(R.id.button9)).check(matches(withText("91")))
        onView(withId(R.id.row1col1)).check(matches(withText("8")))
        onView(withId(R.id.row1col2)).check(matches(withText("2")))
        onView(withId(R.id.row1col3)).check(matches(withText("5")))
        onView(withId(R.id.row1col4)).check(matches(withText("4")))
        onView(withId(R.id.row1col5)).check(matches(withText("7")))
        onView(withId(R.id.row1col6)).check(matches(withText("1")))
        onView(withId(R.id.row1col7)).check(matches(withText("3")))
        onView(withId(R.id.row1col8)).check(matches(withText("9")))
        onView(withId(R.id.row1col9)).check(matches(withText("6")))

        // Update settings from numbers to characters
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Settings")).perform(click())
        onView(withText("Display Settings")).perform(click())
        // The preferences are created internally as a recycler view, so we use the RecyclerViewActions to
        // call each Character Display preference (recycler view list is 0 index based).
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(0, click()))
        onData(`is`("A")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(1, click()))
        onData(`is`("B")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(2, click()))
        onData(`is`("C")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(3, click()))
        onData(`is`("D")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(4, click()))
        onData(`is`("E")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(5, click()))
        onData(`is`("F")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(6, click()))
        onData(`is`("G")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(7, click()))
        onData(`is`("H")).perform(click())
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(8, click()))
        onData(`is`("I")).perform(click())
        pressBack()
        pressBack()

        onView(withId(R.id.button1)).check(matches(withText("A0")))
        onView(withId(R.id.button2)).check(matches(withText("B0")))
        onView(withId(R.id.button3)).check(matches(withText("C0")))
        onView(withId(R.id.button4)).check(matches(withText("D0")))
        onView(withId(R.id.button5)).check(matches(withText("E0")))
        onView(withId(R.id.button6)).check(matches(withText("F0")))
        onView(withId(R.id.button7)).check(matches(withText("G0")))
        onView(withId(R.id.button8)).check(matches(withText("H0")))
        onView(withId(R.id.button9)).check(matches(withText("I1")))
        onView(withId(R.id.row1col1)).check(matches(withText("H")))
        onView(withId(R.id.row1col2)).check(matches(withText("B")))
        onView(withId(R.id.row1col3)).check(matches(withText("E")))
        onView(withId(R.id.row1col4)).check(matches(withText("D")))
        onView(withId(R.id.row1col5)).check(matches(withText("G")))
        onView(withId(R.id.row1col6)).check(matches(withText("A")))
        onView(withId(R.id.row1col7)).check(matches(withText("C")))
        onView(withId(R.id.row1col8)).check(matches(withText("I")))
        onView(withId(R.id.row1col9)).check(matches(withText("F")))

        // Reset the display to numbers
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Settings")).perform(click())
        onView(withText("Display Settings")).perform(click())
        // Reset display item
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(9, click()))
        pressBack()
        pressBack()

        onView(withId(R.id.button1)).check(matches(withText("10")))
        onView(withId(R.id.button2)).check(matches(withText("20")))
        onView(withId(R.id.button3)).check(matches(withText("30")))
        onView(withId(R.id.button4)).check(matches(withText("40")))
        onView(withId(R.id.button5)).check(matches(withText("50")))
        onView(withId(R.id.button6)).check(matches(withText("60")))
        onView(withId(R.id.button7)).check(matches(withText("70")))
        onView(withId(R.id.button8)).check(matches(withText("80")))
        onView(withId(R.id.button9)).check(matches(withText("91")))
        onView(withId(R.id.row1col1)).check(matches(withText("8")))
        onView(withId(R.id.row1col2)).check(matches(withText("2")))
        onView(withId(R.id.row1col3)).check(matches(withText("5")))
        onView(withId(R.id.row1col4)).check(matches(withText("4")))
        onView(withId(R.id.row1col5)).check(matches(withText("7")))
        onView(withId(R.id.row1col6)).check(matches(withText("1")))
        onView(withId(R.id.row1col7)).check(matches(withText("3")))
        onView(withId(R.id.row1col8)).check(matches(withText("9")))
        onView(withId(R.id.row1col9)).check(matches(withText("6")))

        // Update settings from characters to colors
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Settings")).perform(click())
        onView(withText("Display Settings")).perform(click())
        // Enable colors item
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(10, click()))
        onView(withText("Yes")).perform(click())
        pressBack()
        pressBack()

        onView(withId(R.id.button1)).check(matches(withText("0")))
        onView(withId(R.id.button2)).check(matches(withText("0")))
        onView(withId(R.id.button3)).check(matches(withText("0")))
        onView(withId(R.id.button4)).check(matches(withText("0")))
        onView(withId(R.id.button5)).check(matches(withText("0")))
        onView(withId(R.id.button6)).check(matches(withText("0")))
        onView(withId(R.id.button7)).check(matches(withText("0")))
        onView(withId(R.id.button8)).check(matches(withText("0")))
        onView(withId(R.id.button9)).check(matches(withText("1")))
        onView(withId(R.id.row1col1)).check(matches(withText("")))
        onView(withId(R.id.row1col2)).check(matches(withText("")))
        onView(withId(R.id.row1col3)).check(matches(withText("")))
        onView(withId(R.id.row1col4)).check(matches(withText("")))
        onView(withId(R.id.row1col5)).check(matches(withText("")))
        onView(withId(R.id.row1col6)).check(matches(withText("")))
        onView(withId(R.id.row1col7)).check(matches(withText("")))
        onView(withId(R.id.row1col8)).check(matches(withText("")))
        onView(withId(R.id.row1col9)).check(matches(withText("")))

        // Reset display settings
        openActionBarOverflowOrOptionsMenu(getInstrumentation().targetContext)
        onView(withText("Settings")).perform(click())
        onView(withText("Display Settings")).perform(click())
        // Enable colors item
        onView(withId(R.id.recycler_view)).perform(actionOnItemAtPosition<PreferenceViewHolder>(10, click()))
        pressBack()
        pressBack()
        onView(withId(R.id.button1)).check(matches(withText(startsWith("1"))))
        onView(withId(R.id.button2)).check(matches(withText(startsWith("2"))))
        onView(withId(R.id.button3)).check(matches(withText(startsWith("3"))))
        onView(withId(R.id.button4)).check(matches(withText(startsWith("4"))))
        onView(withId(R.id.button5)).check(matches(withText(startsWith("5"))))
        onView(withId(R.id.button6)).check(matches(withText(startsWith("6"))))
        onView(withId(R.id.button7)).check(matches(withText(startsWith("7"))))
        onView(withId(R.id.button8)).check(matches(withText(startsWith("8"))))
        onView(withId(R.id.button9)).check(matches(withText(startsWith("9"))))
        onView(withId(R.id.row1col1)).check(matches(withText("8")))
        onView(withId(R.id.row1col2)).check(matches(withText("2")))
        onView(withId(R.id.row1col3)).check(matches(withText("5")))
        onView(withId(R.id.row1col4)).check(matches(withText("4")))
        onView(withId(R.id.row1col5)).check(matches(withText("7")))
        onView(withId(R.id.row1col6)).check(matches(withText("1")))
        onView(withId(R.id.row1col7)).check(matches(withText("3")))
        onView(withId(R.id.row1col8)).check(matches(withText("9")))
        onView(withId(R.id.row1col9)).check(matches(withText("6")))
    }
}