package dev.mitchdunn.sudoku.model

object Generator {

    /**
     * Generates a ready to play board with the given difficulty.
     * @return the generated board.
     */
    fun generate(difficulty: Difficulty): Board {
        var board: Board
        do {
            board = generate()
        } while(board.difficulty != difficulty)
        return board
    }

    /**
     * Generates a ready to play board of a random difficulty.
     * @return the generated board.
     */
    @JvmStatic
    fun generate(): Board {
        // Generate a completed board randomly by solving an empty board.
        val board = Board()
        Solver.solve(board)
        // Generate a randomly shuffled list of numbers 0-80, these numbers pertain to the cells
        // on the Sudoku board.  The numbers start at the top left, go right to the end of the row
        // then move down to the next row.
        val boardList: MutableList<Int> = ArrayList()
        for (i in 0 until Board.BOARD_SIZE * Board.BOARD_SIZE) {
            boardList.add(i)
        }
        boardList.shuffle()
        // Remove filled in cells one at a time until we get a unique puzzle.
        val solutions: MutableSet<Board> = HashSet()
        var solution: Board = Board(board)
        for (spot in boardList) {
            val row = spot / 9 + 1
            val col = spot % 9 + 1
            val boardToTest = Board(solution)
            boardToTest.clearNumber(row, col)
            Solver.solve(boardToTest, solutions)
            // If we find more than two solutions to a puzzle then the last one we tried is unique
            // so we go with that one.
            if (solutions.size > 1) {
                break
            } else {
                solution = boardToTest
            }
        }
        solution.finalize(board)
        return solution
    }
}