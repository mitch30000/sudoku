package dev.mitchdunn.sudoku.model

enum class Difficulty(var string: String) {
    VERY_EASY("Very Easy"), EASY("Easy"), MEDIUM("Medium"), HARD("Hard");

    override fun toString(): String {
        return string
    }

    companion object {
        /**
         * Returns the difficulty of a board
         * @param board the board to check the difficulty of
         * @return the difficulty of the board
         */
        fun getDifficulty(board: Board): Difficulty {
            // TODO: change difficulty from seeing how many empty spots there are to what moves are needed to solve the puzzle
            return when {
                board.numberAmountRemaining(0) <= 20 -> VERY_EASY
                board.numberAmountRemaining(0) in 21..30 -> EASY
                board.numberAmountRemaining(0) in 31..45 -> MEDIUM
                else -> HARD
            }
        }
    }
}
