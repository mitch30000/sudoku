package dev.mitchdunn.sudoku.model

import java.io.Serializable

class Board: Serializable {
    // Stores the current state of the board which gets modified as the board is solved.
    private var boardArray = IntArray(BOARD_SIZE * BOARD_SIZE)
    // Stores the initial state of the board which is used to determine which numbers
    // can or can not be removed from the board.
    private var initialArray = IntArray(BOARD_SIZE * BOARD_SIZE)
    private var finalized = false
    // Since we create boards that are unique and only contain one solution, we hold that
    // solution here to tell if a number inserted in a specific cell is correct or not
    private var solution = IntArray(BOARD_SIZE * BOARD_SIZE)
    // 1D array which contains the notes that make up the board.  This means that we have taken
    // our normal 1D array of the board, and added another level so that each grid cell has
    // an set of 9 entries which can hold all the possible note values.
    private var notes = BooleanArray(BOARD_SIZE * BOARD_SIZE * BOARD_SIZE)
    var difficulty: Difficulty = Difficulty.VERY_EASY
    var time: Long = 0
    var mistakes: Int = 0
    var hints: Int = 0

    /**
     * Default constructor, creates an empty sudoku board.
     */
    constructor() {
        for (row in 1..BOARD_SIZE) {
            for (col in 1..BOARD_SIZE) {
                boardArray[getIndex(row, col)] = 0
            }
        }
    }

    /**
     * Copy constructor that takes an existing board and creates a copy of it.
     */
    constructor(board: Board) {
        for (row in 1..BOARD_SIZE) {
            for (col in 1..BOARD_SIZE) {
                val number = board.getNumber(row, col)
                boardArray[getIndex(row, col)] = number
            }
        }
    }

    /**
     * Takes a string representing a sudoku board and creates a Board object out of it.
     * @param boardString String of the sudoku board, valid values are 1,2,3,4,5,6,7,8,9
     * and 0 (for blank).  Board must also be 81 characters long.
     */
    constructor(boardString: String?) {
        require(!(boardString == null || BOARD_SIZE * BOARD_SIZE != boardString.length)) { "Sudoku string is not 81 characters long." }
        for (i in boardString.indices) {
            val c = boardString[i]
            val number = Character.getNumericValue(c)
            if (number in 0..BOARD_SIZE) {
                boardArray[i] = number
            } else {
                boardArray = IntArray(BOARD_SIZE * BOARD_SIZE)
                throw IllegalArgumentException("Invalid character at index '$i'.")
            }
        }
    }

    /**
     * Converts the row/col format into an index for the board array.
     * @param row the row number
     * @param col the column number
     * @return the index for the internal board array
     */
    private fun getIndex(row: Int, col: Int): Int {
        return (row - 1) * BOARD_SIZE + (col - 1)
    }

    /**
     * Converts the row/col/note format into an index for the note array.
     * @param row the row number
     * @param col the column number
     * @param note the note to lookup
     * @return the index for the internal note array
     */
    private fun getNoteIndex(row: Int, col: Int, note: Int): Int {
        return ((row - 1) * BOARD_SIZE * BOARD_SIZE) + (col - 1) * BOARD_SIZE + (note - 1)
    }

    /**
     * Sets the board state to final so that we can know which numbers in the board
     * make up the base of the puzzle.  This is needed since we want to be able to
     * add/remove from cells freely while the puzzle is being generated, but not while
     * the puzzle is being solved by the user.
     * @param boardSolution the board object that contains the full solution to the puzzle this
     * objects represents, the solution comes from the boardArray of boardSolution.
     */
    fun finalize(boardSolution: Board) {
        initialArray = boardArray.copyOf()
        solution = boardSolution.boardArray.copyOf()
        finalized = true
        difficulty = Difficulty.getDifficulty(this)
    }

    /**
     * Resets the board to the state it was in when it was finalized, if the board has not been
     * finalized yet then this method does nothing.
     */
    fun resetBoard() {
        if(finalized) {
            boardArray = initialArray.copyOf()
            notes = BooleanArray(BOARD_SIZE * BOARD_SIZE * BOARD_SIZE)
        }
    }

    /**
     * Gets a number from the board.
     * @param row the row to get the number from, can be 1-9.
     * @param col the column to get the number from, can be 1-9.
     * @return the number from the board.
     */
    fun getNumber(row: Int, col: Int): Int {
        return boardArray[getIndex(row, col)]
    }

    /**
     * Checks if a cell on the board is editable.
     * @param row the row to get the cell from, can be 1-9.
     * @param col the column to get the cell from, can be 1-9.
     * @return true if a user can insert/erase that cell, false if
     * that cell is part of the original board state.
     */
    fun isCellEditable(row: Int, col: Int): Boolean {
        return if (finalized) initialArray[getIndex(row, col)] == 0 else true
    }

    /**
     * Clears a number on the board and sets it to blank/zero.
     * @param row the row to clear the number from.
     * @param col the column to clean the number from.
     */
    fun clearNumber(row: Int, col: Int) {
        val number = boardArray[getIndex(row, col)]
        if(isCellEditable(row, col) && number != 0) {
            boardArray[getIndex(row, col)] = 0
        }
    }

    /**
     * Adds a number to the board if possible.  If the number is not forced, then it is inserted only
     * if the solution contains the same number in the same spot.
     * @param number number to add to the board.
     * @param row the row to add the number to.
     * @param col the column to add the number to.
     * @param force if true then the number is added even if it normally could not go in that spot
     * due to row/col/grid restrictions.
     * @return true if the number was added, false if not.
     */
    fun addNumber(number: Int, row: Int, col: Int, force: Boolean): Boolean {
        return if (force || solution[getIndex(row, col)] == number) {
            boardArray[getIndex(row, col)] = number
            if(finalized && !isNumberCorrect(row, col)) {
                mistakes++
            }
            true
        } else {
            false
        }
    }

    fun getNote(number: Int, row: Int, col: Int): Boolean {
        return notes[getNoteIndex(row, col, number)]
    }

    fun getNotes(row: Int, col: Int): BooleanArray {
        return BooleanArray(BOARD_SIZE) {notes[getNoteIndex(row, col, it + 1)]}
    }

    fun addNote(number: Int, row: Int, col: Int) {
        notes[getNoteIndex(row, col, number)] = true
    }

    fun clearNote(number: Int) {
        for (i in 1..BOARD_SIZE) {
            for (j in 1..BOARD_SIZE) {
                notes[getNoteIndex(i, j, number)] = false
            }
        }
    }

    fun clearNote(number: Int, row: Int, col: Int) {
        notes[getNoteIndex(row, col, number)] = false
    }

    fun clearAllNotes(row: Int, col: Int) {
        for (number in 1..BOARD_SIZE) {
            notes[getNoteIndex(row, col, number)] = false
        }

    }

    fun getHint(row: Int, col: Int) {
        val number = solution[getIndex(row, col)]
        addNumber(number, row, col, false)
        hints++
    }

    /**
     * Returns the number of spots remaining of the given number.
     * @param number the number (0-9, 0 for blank cells) to check the remaining amount of.
     * @return a number showing the amount of spots left on the board for a specific number.  Once
     * a board has been fully filled in, this method should return 0 for all numbers 0-9
     */
    fun numberAmountRemaining(number: Int): Int {
        val count = boardArray.count { int -> int == number }
        // We can just count the number of zeroes to get the amount of blank spaces remaining, but
        // we have to subtract the other numbers against the max amount of that number that can
        // be on the board.
        return if (number == 0) count else BOARD_SIZE - count
    }

    /**
     * Returns a boolean representing if the number on the board is the correct number given the solution.
     * @param row the row to check.
     * @param col the column to check.
     * @return true if the board is finalized and the number belongs in the given spot, false otherwise.
     */
    fun isNumberCorrect(row: Int, col: Int): Boolean {
        return finalized && solution[getIndex(row, col)] == boardArray[getIndex(row, col)]
    }

    /**
     * Checks if a number can go in a certain spot by seeing if that number already exists in the
     * same row, column and grid.
     * @param number number to check can be added to the board.
     * @param row the row to check for the number.
     * @param col the column to check for the number.
     * @return true if the number can be added in the spot, false if not.
     */
    fun canNumberGoInPlace(number: Int, row: Int, col: Int): Boolean {
        // Check rows and columns
        for (i in 1..BOARD_SIZE) {
            if (boardArray[getIndex(row, i)] == number || boardArray[getIndex(i, col)] == number) {
                return false
            }
        }

        // Check grids
        val gridRow = (row - 1) / GRID_SIZE
        val gridCol = (col - 1) / GRID_SIZE
        for (i in gridRow * GRID_SIZE + 1 until gridRow * GRID_SIZE + GRID_SIZE + 1) {
            for (j in gridCol * GRID_SIZE + 1 until gridCol * GRID_SIZE + GRID_SIZE + 1) {
                if (boardArray[getIndex(i, j)] == number) {
                    return false
                }
            }
        }
        return true
    }

    /**
     * Checks if a row is valid and contains all numbers 1-9 with no repeats.
     * @param row the row to check.
     * @return true if row is valid, false if not
     */
    fun isValidRow(row: Int): Boolean {
        val rowSet: MutableSet<Int> = HashSet()
        for (i in 1..BOARD_SIZE) {
            rowSet.add(boardArray[getIndex(row, i)])
        }
        return BOARD_SIZE == rowSet.size && !rowSet.contains(0)
    }

    /**
     * Checks if a column is valid and contains all numbers 1-9 with no repeats.
     * @param col the column to check.
     * @return true if column is valid, false if not
     */
    fun isValidCol(col: Int): Boolean {
        val colSet: MutableSet<Int> = HashSet()
        for (i in 1..BOARD_SIZE) {
            colSet.add(boardArray[getIndex(i, col)])
        }
        return BOARD_SIZE == colSet.size && !colSet.contains(0)
    }

    /**
     * Checks if a grid is valid or not, this means checking that the grid has the numbers 1-9 in it.
     * @param grid the grid to check, the grid number starts at 1 for the top left, 2 for top middle,
     * 3 for top right, 4 for middle left, etc.
     * @return true if grid is valid, false if not.
     */
    fun isValidGrid(grid: Int): Boolean {
        val gridSet: MutableSet<Int> = HashSet()
        val gridRow = (grid - 1) / GRID_SIZE * GRID_SIZE + 1
        val gridCol = (grid - 1) % GRID_SIZE * GRID_SIZE + 1
        for (i in gridRow until gridRow + GRID_SIZE) {
            for (j in gridCol until gridCol + GRID_SIZE) {
                gridSet.add(boardArray[getIndex(i, j)])
            }
        }
        return BOARD_SIZE == gridSet.size && !gridSet.contains(0)
    }

    /**
     * Checks that the puzzle is valid and solved.
     * @return true if the puzzle is valid and solved, false if not.
     */
    val isValidPuzzle: Boolean
        get() {
            for (i in 1..BOARD_SIZE) {
                if (!isValidRow(i) || !isValidCol(i) || !isValidGrid(i)) {
                    return false
                }
            }
            return true
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val board = other as Board
        return boardArray.contentEquals(board.boardArray)
    }

    override fun hashCode(): Int {
        return boardArray.contentHashCode()
    }

    override fun toString(): String {
        return boardArray.joinToString(separator = "")
    }

    /**
     * Pretty print representation of the board.
     * @return pretty print string representing the board.
     */
    fun prettyPrint(): String {
        val sb = StringBuilder()
        for (i in 1..BOARD_SIZE) {
            if (i % GRID_SIZE == 1) {
                sb.append("+---+---+---+\n|")
            } else {
                sb.append("|")
            }
            for (j in 1..BOARD_SIZE) {
                if (j != 1 && j % GRID_SIZE == 1) {
                    sb.append("|")
                }
                sb.append(boardArray[getIndex(i, j)])
            }
            sb.append("|\n")
        }
        sb.append("+---+---+---+")
        return sb.toString()
    }

    companion object {
        const val GRID_SIZE = 3
        const val BOARD_SIZE = 9
    }
}