package dev.mitchdunn.sudoku.model

object Solver {
    private val NUMBERS: MutableList<Int> = mutableListOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
    private var solvingFinished = false

    /**
     * Attempts to solve a given board, will find a solution (if it exists) then return from the method call.  If no
     * solution is found then the board will remain in the state it was at the start of the method call.
     * @param board the board to solve
     * @return true if a solution was found, false if not
     */
    fun solve(board: Board): Boolean {
        solvingFinished = false
        findNumber(board, 1, 1, false, HashSet())
        return board.isValidPuzzle
    }

    /**
     * Attempts to solve a given board, will find all the solutions to a given board.  If no solution is found
     * then the board will remain in the state it was at the start of the method call.
     * @param board the board to solve
     * @param solutions the Set to store the solutions in
     * @return true if any solutions were found, false if not
     */
    fun solve(board: Board, solutions: MutableSet<Board>): Boolean {
        solvingFinished = false
        solutions.clear()
        findNumber(board, 1, 1, true, solutions)
        return solutions.isNotEmpty()
    }

    /**
     * Attempts to find a valid number that can go in a specific row/column on the board.
     * @param boardToComplete the board to find the number for.
     * @param row the row that the number will go in.
     * @param col the column that the number will go in.
     * @param findMultipleSolutions if true then the recursion won't stop upon finding a solution, and
     * all solutions will be added to the solution list.
     * @param solutions the collection that contains all the current solutions for the board.
     */
    private fun findNumber(boardToComplete: Board, row: Int, col: Int, findMultipleSolutions: Boolean, solutions: MutableSet<Board>) {
        if (row > Board.BOARD_SIZE) {
            // If we get here then we have a valid puzzle, we only want to continue if we want
            // to attempt to find multiple solutions (such as in a bid to see if the puzzle is unique).
            if (findMultipleSolutions) {
                val finishedBoard = Board(boardToComplete)
                solutions.add(finishedBoard)
                return
            } else {
                solvingFinished = true
                return
            }
        }
        if (boardToComplete.getNumber(row, col) != 0) {
            // If this spot is already filled in, then move to next.
            if (!solvingFinished) {
                findNextNumber(boardToComplete, row, col, findMultipleSolutions, solutions)
            }
        } else {
            // If we can add the number then move to the next spot, otherwise keep trying different
            // numbers.  If no numbers fit then set the spot to 0 and let the method finish.  Since
            // it is a recursive method this will take us up back a level and get us to the previous
            // spot where we can try another number and go from there.
            val numbers = NUMBERS.toMutableList()
            numbers.shuffle()
            for (i in NUMBERS.indices) {
                if (boardToComplete.canNumberGoInPlace(numbers[i], row, col)) {
                    boardToComplete.addNumber(numbers[i], row, col, true)
                    if (!solvingFinished) {
                        findNextNumber(boardToComplete, row, col, findMultipleSolutions, solutions)
                    }
                }
            }
            if (!solvingFinished) {
                boardToComplete.clearNumber(row, col)
            }
        }
    }

    /**
     * Traverses to the next number on the board that we want to try and calls findNumber() for it.  This
     * is done by going left to right in a row, then once the end of the row is reached going down a row and
     * repeating until we get have found a number for the bottom right spot.
     * @param board the board to find the next number for.
     * @param row the row that we are currently on.
     * @param col the column that we are currently on.
     * @param findMultipleSolutions if true then the recursion won't stop upon finding a solution, and
     * all solutions will be added to the solution list.
     * @param solutions the collection that contains all the current solutions for the board.
     */
    private fun findNextNumber(board: Board, row: Int, col: Int, findMultipleSolutions: Boolean, solutions: MutableSet<Board>) {
        if (col < Board.BOARD_SIZE) {
            findNumber(board, row, col + 1, findMultipleSolutions, solutions)
        } else {
            findNumber(board, row + 1, 1, findMultipleSolutions, solutions)
        }
    }
}