package dev.mitchdunn.sudoku.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import dev.mitchdunn.R
import dev.mitchdunn.sudoku.model.Board
import dev.mitchdunn.sudoku.model.Generator

class SudokuBoardViewModel(application: Application) : AndroidViewModel(application), SharedPreferences.OnSharedPreferenceChangeListener {
    private val context: Context = application.applicationContext
    var initialized: Boolean = false
    val board: MutableLiveData<Board> = MutableLiveData()
    val showTimer: MutableLiveData<Boolean> = MutableLiveData()
    val showMistakes: MutableLiveData<Boolean> = MutableLiveData()
    val solved: MutableLiveData<Boolean> = MutableLiveData()
    val buttonNames: MutableLiveData<MutableList<String>> = MutableLiveData()
    private var prefs: SharedPreferences
    private var showError: String
    private var removeNotes: Boolean

    init {
        board.value = Generator.generate()
        showTimer.value = true
        showMistakes.value = true
        solved.value = false
        buttonNames.value = mutableListOf()
        for(i in 1..9) {
            buttonNames.value!!.add(i.toString())
        }
        prefs = context.getSharedPreferences(context.packageName + "_preferences", Context.MODE_PRIVATE)
        // Update the button display data bindings on creation and anytime the shared preferences change.
        updateButtonDisplays()
        prefs.registerOnSharedPreferenceChangeListener(this)
        showTimer.value = prefs.getBoolean("show_timer", true)
        showError = prefs.getString("show_errors", context.resources.getString(R.string.show_no_error)).toString()
        if (showError == context.resources.getString(R.string.show_no_error)) {
            showMistakes.value = false
        } else {
            showMistakes.value = prefs.getBoolean("show_mistakes", true)
        }
        removeNotes = prefs.getBoolean("remove_notes", false)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        updateButtonDisplays()
        showTimer.value = prefs.getBoolean("show_timer", true)
        showError = prefs.getString("show_errors", context.resources.getString(R.string.show_no_error)).toString()
        if (showError == context.resources.getString(R.string.show_no_error)) {
            showMistakes.value = false
        } else {
            showMistakes.value = prefs.getBoolean("show_mistakes", true)
        }
        removeNotes = prefs.getBoolean("remove_notes", false)
    }

    private fun updateButtonDisplays() {
        for(number in 1..9) {
            val buttonName: String = prefs.getString("button_" + number + "_display", number.toString()).toString()
            buttonNames.value?.set(number - 1, buttonName)
        }
    }

    fun createNewGame(newBoard: Board) {
        board.value = newBoard
        solved.value = false
    }

    fun getTime(): Long {
        return board.value?.time!!
    }

    fun updateTime(time: Long) {
        board.value?.time = time
    }

    fun getHint(row: Int, col: Int) {
        val boardObj: Board? = board.value
        boardObj?.getHint(row, col)
        board.value = boardObj
        if (board.value!!.isValidPuzzle) {
            solved.value = true
        }
    }

    fun addNumber(number: Int, row: Int, col: Int) {
        val boardObj: Board? = board.value
        // TODO: Would probably be ideal to move this check into the Board object itself, but that
        // would require some reworking so generation and other functions aren't affected.
        if (boardObj?.getNumber(row, col) == 0) {
            if (showError == context.resources.getString(R.string.do_not_insert)) {
                boardObj.addNumber(number, row, col, false)
            } else {
                boardObj.addNumber(number, row, col, true)
            }
            board.value = boardObj
            if (board.value!!.isValidPuzzle) {
                solved.value = true
            }
            if (removeNotes && board.value!!.numberAmountRemaining(number) == 0) {
                board.value!!.clearNote(number)
            }
        }
    }

    fun clearNumber(row: Int, col: Int) {
        val boardObj: Board? = board.value
        boardObj?.clearNumber( row, col)
        board.value = boardObj
    }

    fun getNote(number: Int, row: Int, col: Int): Boolean {
        return board.value?.getNote(number, row, col)!!
    }

    fun addNote(number: Int, row: Int, col: Int) {
        val boardObj: Board? = board.value
        boardObj?.addNote(number, row, col)
        board.value = boardObj
    }

    fun clearNote(number: Int, row: Int, col: Int) {
        val boardObj: Board? = board.value
        boardObj?.clearNote(number, row, col)
        board.value = boardObj
    }

    fun clearAllNotes(row: Int, col: Int) {
        val boardObj: Board? = board.value
        boardObj?.clearAllNotes(row, col)
        board.value = boardObj
    }

    fun resetBoard() {
        if (solved.value!!.not()) {
            val boardObj: Board? = board.value
            boardObj?.resetBoard()
            board.value = boardObj
        }
    }
}