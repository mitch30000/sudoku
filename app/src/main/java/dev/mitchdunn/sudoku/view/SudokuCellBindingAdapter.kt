package dev.mitchdunn.sudoku.view

import android.content.SharedPreferences
import android.util.TypedValue
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.preference.PreferenceManager
import dev.mitchdunn.R

private var prefs: SharedPreferences? = null

@BindingAdapter("sudokuCell", "sudokuCellNotes", "sudokuCellEditable", "sudokuCellCorrect")
fun setSudokuCell(view: TextView, number: Int, notes: BooleanArray, isEditable: Boolean, isCorrect: Boolean) {
    if(prefs == null) {
        prefs = PreferenceManager.getDefaultSharedPreferences(view.context)
    }

    view.text = ""
    view.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.context.resources.getDimension(R.dimen.sudokuCellTextSize))

    val enabledColors = prefs?.getBoolean("enable_colors", false)!!
    if (!enabledColors) {
        view.text = if (number == 0) {
            // If no notes then just set the cell to blank, otherwise fill in the notes.
            if(!notes.contains(true)) {
                " "
            } else {
                // Turn the note int array into a string that we can feed into the view, changing any falses into blanks
                val sb = StringBuilder()
                for(index in notes.indices) {
                    if(notes[index]) {
                        sb.append(index+1)
                    } else {
                        sb.append(" ")
                    }
                    if(index == 2 || index == 5) {
                        sb.append("\n")
                    }
                }
                view.setTextSize(TypedValue.COMPLEX_UNIT_PX, view.context.resources.getDimension(R.dimen.sudokuCellNoteTextSize))
                sb.toString()
            }
        } else {
            prefs?.getString("button_" + number + "_display", number.toString())
        }
        // We set the backgrounds manually here (in addition to the OnFocusChangeListener) in case
        // we switch back from colors to numbers/characters, that way the colors disappear properly.
        if (view.hasFocus()) {
            view.setBackgroundResource(R.drawable.sudoku_cell_selected)
        } else {
            view.setBackgroundResource(R.drawable.sudoku_cell_default)
        }
    } else {
        val color = when (number) {
            1 -> R.drawable.sudoku_cell_red
            2 -> R.drawable.sudoku_cell_orange
            3 -> R.drawable.sudoku_cell_yellow
            4 -> R.drawable.sudoku_cell_green
            5 -> R.drawable.sudoku_cell_blue
            6 -> R.drawable.sudoku_cell_purple
            7 -> R.drawable.sudoku_cell_pink
            8 -> R.drawable.sudoku_cell_brown
            9 -> R.drawable.sudoku_cell_gray
            else -> R.drawable.sudoku_cell_default
        }
        view.setBackgroundResource(color)
    }

    // Set the text color
    if(isEditable) {
        val showError = prefs?.getString("show_errors", view.context.resources.getString(R.string.show_no_error)).toString()
        if (showError == view.context.resources.getString(R.string.show_red_character)) {
            if(notes.contains(true) || isCorrect) {
                view.setTextColor(ContextCompat.getColor(view.context, R.color.colorPrimary))
            } else {
                view.setTextColor(ContextCompat.getColor(view.context, android.R.color.holo_red_dark))
            }
        } else {
            view.setTextColor(ContextCompat.getColor(view.context, R.color.colorPrimary))
        }
    } else {
        view.setTextColor(ContextCompat.getColor(view.context, R.color.defaultTextColor))
    }
}
