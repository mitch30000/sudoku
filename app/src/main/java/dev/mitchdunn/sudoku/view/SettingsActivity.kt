package dev.mitchdunn.sudoku.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import dev.mitchdunn.R


class SettingsActivity : AppCompatActivity(),
        PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.settings, SettingsFragment())
                .commit()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onPreferenceStartFragment(caller: PreferenceFragmentCompat, pref: Preference): Boolean {
        val fragment = supportFragmentManager.fragmentFactory.instantiate(
                classLoader,
                pref.fragment)
        fragment.arguments = pref.extras
        fragment.setTargetFragment(caller, 0)
        supportFragmentManager.beginTransaction()
                .replace(R.id.settings, fragment)
                .addToBackStack(null)
                .commit()
        return true
    }

    class SettingsFragment : PreferenceFragmentCompat(), Preference.OnPreferenceChangeListener {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)

            findPreference<ListPreference>("show_errors")?.onPreferenceChangeListener = this
            // If show errors is off then we disable the show mistakes switch since show mistakes
            // will be set to false regardless.
            findPreference<Preference?>("show_mistakes")?.isEnabled = findPreference<ListPreference>("show_errors")?.value != context?.resources?.getString(R.string.show_no_error)
        }

        override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
            findPreference<Preference?>("show_mistakes")?.isEnabled = newValue != context?.resources?.getString(R.string.show_no_error)
            return true
        }
    }
}