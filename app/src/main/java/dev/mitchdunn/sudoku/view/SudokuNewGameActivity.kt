package dev.mitchdunn.sudoku.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout.LayoutParams
import androidx.appcompat.app.AppCompatActivity
import dev.mitchdunn.R
import dev.mitchdunn.sudoku.model.Difficulty
import dev.mitchdunn.sudoku.model.Generator
import kotlinx.android.synthetic.main.activity_sudoku_new_game.newGameButtonLayout
import kotlinx.android.synthetic.main.activity_sudoku_new_game.sudokuToolbar


class SudokuNewGameActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sudoku_new_game)
        setSupportActionBar(sudokuToolbar)
        title = getString(R.string.title_new_game)
        for (difficulty: Difficulty in Difficulty.values()) {
            val button = Button(this)
            button.tag = difficulty
            button.text = difficulty.toString()
            button.layoutParams = LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
            button.setOnClickListener(this)
            newGameButtonLayout.addView(button)
        }
    }

    override fun onClick(view: View) {
        val board = Generator.generate(view.tag as Difficulty)
        val intent = Intent(this, SudokuSolveActivity::class.java)
        intent.putExtra("board", board)
        startActivity(intent)
        finish()
    }
}