package dev.mitchdunn.sudoku.view

import android.content.SharedPreferences
import android.text.Html
import android.widget.Button
import androidx.databinding.BindingAdapter
import androidx.preference.PreferenceManager
import dev.mitchdunn.R

private var prefs: SharedPreferences? = null

@BindingAdapter("sudokuButtonName", "sudokuButtonAmountRemaining")
fun setSudokuButton(view: Button, name: String, amountRemaining: Int) {
    if(prefs == null) {
        prefs = PreferenceManager.getDefaultSharedPreferences(view.context)
    }

    if(prefs?.getBoolean("enable_colors", false)!!) {
        view.text = amountRemaining.toString()
        view.background = when (view.tag) {
            "1" -> view.context.getDrawable(R.drawable.button_red_inset)
            "2" -> view.context.getDrawable(R.drawable.button_orange_inset)
            "3" -> view.context.getDrawable(R.drawable.button_yellow_inset)
            "4" -> view.context.getDrawable(R.drawable.button_green_inset)
            "5" -> view.context.getDrawable(R.drawable.button_blue_inset)
            "6" -> view.context.getDrawable(R.drawable.button_purple_inset)
            "7" -> view.context.getDrawable(R.drawable.button_pink_inset)
            "8" -> view.context.getDrawable(R.drawable.button_brown_inset)
            else -> view.context.getDrawable(R.drawable.button_gray_inset)
        }
    } else {
        view.text = Html.fromHtml("$name<sup><small>$amountRemaining</sup></small>", Html.FROM_HTML_MODE_LEGACY)
        view.background = null
    }
}