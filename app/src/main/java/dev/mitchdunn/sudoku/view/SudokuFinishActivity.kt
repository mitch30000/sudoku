package dev.mitchdunn.sudoku.view

import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import androidx.appcompat.app.AppCompatActivity
import com.plattysoft.leonids.ParticleSystem
import dev.mitchdunn.R
import dev.mitchdunn.sudoku.model.Board
import kotlinx.android.synthetic.main.activity_sudoku_finish.finishDifficulty
import kotlinx.android.synthetic.main.activity_sudoku_finish.finishHints
import kotlinx.android.synthetic.main.activity_sudoku_finish.finishMistakes
import kotlinx.android.synthetic.main.activity_sudoku_finish.finishTime
import kotlinx.android.synthetic.main.activity_sudoku_new_game.sudokuToolbar
import kotlin.random.Random

class SudokuFinishActivity : AppCompatActivity() {
    private val fireworkList: IntArray = intArrayOf(R.drawable.blue_firework,
            R.drawable.green_firework,
            R.drawable.purple_firework,
            R.drawable.red_firework,
            R.drawable.yellow_firework)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sudoku_finish)
        setSupportActionBar(sudokuToolbar)
        title = getString(R.string.title_finish)
        val board = intent.getSerializableExtra("finishedBoard") as Board
        finishDifficulty.text = getString(R.string.finish_difficulty, board.difficulty)
        finishTime.base = SystemClock.elapsedRealtime() + board.time
        finishMistakes.text = getString(R.string.finish_mistakes, board.mistakes)
        finishHints.text = getString(R.string.finish_hints, board.hints)
    }

    override fun onStart() {
        super.onStart()
        val delay: Long = 250
        val handler = Handler()
        handler.post(object : Runnable {
            override fun run() {
                showFirework()
                handler.postDelayed(this, delay)
            }
        })
    }

    fun newGame(view: View) {
        startActivity(Intent(this, SudokuNewGameActivity::class.java))
        finish()
    }

    private fun showFirework() {
        // The particle system library we use doesn't have a good function for doing a 'oneShot()' at
        // a X/Y location, so we create a view that randomly sets the X/Y to be near the middle of
        // the screen, then add it to the root content so that the library can pull the location in
        // order to know where the particles/fireworks should originate from.
        val randomView = View(this)
        val width = Resources.getSystem().displayMetrics.widthPixels
        val height = Resources.getSystem().displayMetrics.heightPixels
        randomView.x = (Random.nextFloat() * (width / 2)) + (width / 4)
        randomView.y = (Random.nextFloat() * (height / 2)) + (height / 4)
        val root: ViewGroup = findViewById(android.R.id.content)
        root.addView(randomView)
        ParticleSystem(this, 100, fireworkList.random(), 800)
                .setScaleRange(0.7f, 1.3f)
                .setSpeedRange(0.1f, 0.25f)
                .setRotationSpeedRange(90f, 180f)
                .setFadeOut(200, AccelerateInterpolator())
                .oneShot(randomView, 70)
        root.removeView(randomView)
    }
}