package dev.mitchdunn.sudoku.view

import android.content.Context
import android.content.SharedPreferences
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.util.TypedValue
import android.view.View
import androidx.appcompat.widget.AppCompatTextView
import androidx.preference.PreferenceManager
import dev.mitchdunn.R


class SudokuCellView(context: Context, attrs: AttributeSet) :
        AppCompatTextView(context, attrs), View.OnFocusChangeListener {

    var row: Int = 0
    var col: Int = 0
    private val prefs: SharedPreferences
    private val selected: Drawable? = context.getDrawable(R.drawable.sudoku_cell_selected)
    private val default: Drawable? = context.getDrawable(R.drawable.sudoku_cell_default)

    init {
        // Setting the row and col like this will only work as long as
        // the ID follows the id/rowXcolY format
        val id = resources.getResourceName(this.id)
        val rowColIntList = id.split('/')[1].removePrefix("row").split("col")
        row = rowColIntList[0].toInt()
        col = rowColIntList[1].toInt()
        prefs = PreferenceManager.getDefaultSharedPreferences(context)
        onFocusChangeListener = this
        background = default
        contentDescription = "The row $row column $col cell for the Sudoku board"
    }

    override fun onFocusChange(view: View?, hasFocus: Boolean) {
        if (!prefs.getBoolean("enable_colors", false)) {
            background = if (hasFocus) {
                selected
            } else {
                default
            }
        } else {
            val strokeWidth = if (hasFocus) {
                view?.resources?.getDimensionPixelSize(R.dimen.sudokuCellColorSelectStrokeWidth)!!
            } else {
                view?.resources?.getDimensionPixelSize(R.dimen.sudokuCellStrokeWidth)!!
            }
            (view.background?.mutate() as GradientDrawable).setStroke(strokeWidth, resources.getColor(android.R.color.black))
        }
    }
}