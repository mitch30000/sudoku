package dev.mitchdunn.sudoku.view

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.preference.ListPreference
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import dev.mitchdunn.R

class DisplaySettingsFragment : PreferenceFragmentCompat(), Preference.OnPreferenceChangeListener {
    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.display_settings, rootKey)

        // Reset the display to the default 1-9 numbers when the button is hit
        preferenceManager.findPreference<Preference?>("reset_display")?.onPreferenceClickListener = Preference.OnPreferenceClickListener {
            with(preferenceManager.sharedPreferences.edit()) {
                for (i in 1..9) {
                    putString("button_${i}_display", i.toString())
                }
                commit()
            }
            true
        }

        preferenceManager.findPreference<SwitchPreference>("enable_colors")?.onPreferenceClickListener = Preference.OnPreferenceClickListener { it as SwitchPreference
            if (it.isChecked) {
                AlertDialog.Builder(requireContext())
                        .setTitle(R.string.enable_colors_title)
                        .setMessage(R.string.enable_colors_message)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(R.string.option_yes) { _, _ ->
                            with(preferenceManager.sharedPreferences.edit()) {
                                putString("button_1_display", getString(R.string.color_red))
                                putString("button_2_display", getString(R.string.color_orange))
                                putString("button_3_display", getString(R.string.color_yellow))
                                putString("button_4_display", getString(R.string.color_green))
                                putString("button_5_display", getString(R.string.color_blue))
                                putString("button_6_display", getString(R.string.color_purple))
                                putString("button_7_display", getString(R.string.color_pink))
                                putString("button_8_display", getString(R.string.color_brown))
                                putString("button_9_display", getString(R.string.color_gray))
                                commit()
                            }
                        }
                        .setNegativeButton(R.string.option_cancel) { _, _ ->
                            it.isChecked = false
                        }
                        .show()
            } else {
                with(preferenceManager.sharedPreferences.edit()) {
                    for (i in 1..9) {
                        putString("button_${i}_display", i.toString())
                    }
                    commit()
                }
            }
            true
        }

        for (i in 1..9) {
            findPreference<ListPreference>("button_${i}_display")?.onPreferenceChangeListener = this
        }
    }

    override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
        val newValueString: String = newValue as String
        val listPref: ListPreference = preference as ListPreference
        // If the same value as selected then return, otherwise check that the new value isn't being
        // used in any of the other displays currently, if not then set the value, if so then show
        // an alert to the user since the character is in use already.
        if (listPref.value == newValueString) {
            return true
        } else if (findPreference<ListPreference>("button_1_display")?.value != newValueString &&
                findPreference<ListPreference>("button_2_display")?.value != newValueString &&
                findPreference<ListPreference>("button_3_display")?.value != newValueString &&
                findPreference<ListPreference>("button_4_display")?.value != newValueString &&
                findPreference<ListPreference>("button_5_display")?.value != newValueString &&
                findPreference<ListPreference>("button_6_display")?.value != newValueString &&
                findPreference<ListPreference>("button_7_display")?.value != newValueString &&
                findPreference<ListPreference>("button_8_display")?.value != newValueString &&
                findPreference<ListPreference>("button_9_display")?.value != newValueString) {
            return true
        } else {
            AlertDialog.Builder(requireContext())
                    .setTitle(R.string.duplicate_display_title)
                    .setMessage(getString(R.string.duplicate_display_message, newValueString))
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show()
            return false
        }
    }
}