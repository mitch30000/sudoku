package dev.mitchdunn.sudoku.view

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.SystemClock
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.preference.PreferenceManager
import dev.mitchdunn.R
import dev.mitchdunn.databinding.ActivitySudokuSolveBinding
import dev.mitchdunn.sudoku.model.Board
import dev.mitchdunn.sudoku.viewmodel.SudokuBoardViewModel
import kotlinx.android.synthetic.main.activity_sudoku_solve.button1
import kotlinx.android.synthetic.main.activity_sudoku_solve.button2
import kotlinx.android.synthetic.main.activity_sudoku_solve.button3
import kotlinx.android.synthetic.main.activity_sudoku_solve.button4
import kotlinx.android.synthetic.main.activity_sudoku_solve.button5
import kotlinx.android.synthetic.main.activity_sudoku_solve.button6
import kotlinx.android.synthetic.main.activity_sudoku_solve.button7
import kotlinx.android.synthetic.main.activity_sudoku_solve.button8
import kotlinx.android.synthetic.main.activity_sudoku_solve.button9
import kotlinx.android.synthetic.main.activity_sudoku_solve.buttonErase
import kotlinx.android.synthetic.main.activity_sudoku_solve.buttonHint
import kotlinx.android.synthetic.main.activity_sudoku_solve.buttonNotes
import kotlinx.android.synthetic.main.activity_sudoku_solve.sudokuToolbar
import kotlinx.android.synthetic.main.activity_sudoku_solve.timer
import java.io.File
import java.io.FileOutputStream
import java.io.ObjectOutputStream


class SudokuSolveActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    private val model: SudokuBoardViewModel by viewModels()

    private val numberButtons: MutableList<Button> = ArrayList()

    private lateinit var binding: ActivitySudokuSolveBinding

    private lateinit var prefs: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sudoku_solve)

        // We only want to run this logic once, when we come to here from the SudokuNewGameActivity,
        // that way onCreate can be called multiple times (such as when rotating the screen) but the
        // game is initialized/created only once.
        if(!model.initialized) {
            val board = intent.getSerializableExtra("board") as Board
            model.createNewGame(board)
            model.initialized = true
        }

        title = model.board.value?.difficulty.toString()
        model.solved.observe(this, Observer { solved ->
            if(solved) {
                Toast.makeText(this, "Solved!", Toast.LENGTH_SHORT).show()
                finishGame()
                File(applicationContext.filesDir, "savedBoardGame").delete()
                val intent = Intent(this, SudokuFinishActivity::class.java)
                intent.putExtra("finishedBoard", model.board.value)
                startActivity(intent)
                finish()
            }
        })

        binding.viewModel = model
        binding.lifecycleOwner = this
        numberButtons.add(button1)
        numberButtons.add(button2)
        numberButtons.add(button3)
        numberButtons.add(button4)
        numberButtons.add(button5)
        numberButtons.add(button6)
        numberButtons.add(button7)
        numberButtons.add(button8)
        numberButtons.add(button9)
        setSupportActionBar(sudokuToolbar)
        prefs = PreferenceManager.getDefaultSharedPreferences(this)
        prefs.registerOnSharedPreferenceChangeListener(this)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.menuNewGame -> {
            model.initialized = false
            startActivity(Intent(this, SudokuNewGameActivity::class.java))
            finish()
            true
        }
        R.id.menuResetGame -> {
            AlertDialog.Builder(this)
                    .setTitle(R.string.reset_board_title)
                    .setMessage(R.string.reset_board_message)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(R.string.option_yes) { _, _ ->
                        model.resetBoard()
                    }
                    .setNegativeButton(R.string.option_cancel) { _, _ ->
                    }
                    .show()
            true
        }
        R.id.menuSettings -> {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
            true
        }
        else -> {
            super.onOptionsItemSelected(item)
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        menu?.findItem(R.id.menuResetGame)?.isEnabled = !model.solved.value!!
        return super.onPrepareOptionsMenu(menu)
    }

    fun onNumberButtonClick(view: View) {
        if (this.currentFocus != null && this.currentFocus is SudokuCellView) {
            val cell: SudokuCellView = this.currentFocus as SudokuCellView
            // Check if we are taking notes or inserting actual numbers/characters/etc
            if (buttonNotes.isChecked) {
                if (view.id == R.id.buttonErase) {
                    model.clearAllNotes(cell.row, cell.col)
                } else {
                    if(!model.getNote(Integer.parseInt(view.tag as String), cell.row, cell.col)) {
                        model.addNote(Integer.parseInt(view.tag as String), cell.row, cell.col)
                    } else {
                        model.clearNote(Integer.parseInt(view.tag as String), cell.row, cell.col)
                    }
                }
            } else {
                if (view.id == R.id.buttonErase) {
                    model.clearNumber(cell.row, cell.col)
                } else {
                    // Use the tag of the button to figure out which position (1-9)
                    // it holds in the button/board layout.  We do this since the user
                    // maybe viewing the cell characters as something other than the
                    // normal 1-9, but the Board object deals with 1-9 integers.
                    model.addNumber(Integer.parseInt(view.tag as String), cell.row, cell.col)
                    model.clearAllNotes(cell.row, cell.col)
                }
            }
        }
    }

    fun onHintClick(view: View) {
        if (this.currentFocus != null && this.currentFocus is SudokuCellView) {
            val cell: SudokuCellView = this.currentFocus as SudokuCellView
            model.getHint(cell.row, cell.col)
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        // We may be resuming after leaving the settings activity, where the user
        // may had changed the character display.  In that case we invalidate the
        // bindings so they can be refreshed for the new display (the model doesn't
        // actually change when changing settings which is why we do this part manually).
        binding.invalidateAll()
    }

    private fun finishGame() {
        for( button: Button in numberButtons) {
            button.isClickable = false
        }
        buttonErase.isClickable = false
        buttonHint.isClickable = false
        buttonNotes.isClickable = false
        stopTimer()
    }

    private fun stopTimer() {
        model.updateTime(timer.base - SystemClock.elapsedRealtime())
        timer.stop()
    }

    override fun onResume() {
        super.onResume()
        if (!model.solved.value!!) {
            timer.base = SystemClock.elapsedRealtime() + model.getTime()
            timer.start()
        }
    }

    override fun onPause() {
        super.onPause()
        stopTimer()
        if (!model.solved.value!!) {
            val outputStream = ObjectOutputStream(FileOutputStream(File(applicationContext.filesDir, "savedBoardGame")))
            outputStream.writeObject(model.board.value)
            outputStream.flush()
            outputStream.close()
        }
    }
}