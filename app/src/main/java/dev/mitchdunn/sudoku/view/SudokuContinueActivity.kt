package dev.mitchdunn.sudoku.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import dev.mitchdunn.R
import dev.mitchdunn.sudoku.model.Board
import kotlinx.android.synthetic.main.activity_sudoku_continue.continueButton
import kotlinx.android.synthetic.main.activity_sudoku_new_game.sudokuToolbar
import java.io.File
import java.io.FileInputStream
import java.io.ObjectInputStream


class SudokuContinueActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sudoku_continue)
        setSupportActionBar(sudokuToolbar)
        title = getString(R.string.title_continue)
        continueButton.isEnabled = applicationContext.fileList().contains("savedBoardGame")
    }

    fun newGame(view: View) {
        startActivity(Intent(this, SudokuNewGameActivity::class.java))
        finish()
    }

    fun continueGame(view: View) {
        val inputStream = ObjectInputStream(FileInputStream(File(applicationContext.filesDir, "savedBoardGame")))
        val board: Board = inputStream.readObject() as Board
        inputStream.close()
        val intent = Intent(this, SudokuSolveActivity::class.java)
        intent.putExtra("board", board)
        startActivity(intent)
        finish()
    }
}