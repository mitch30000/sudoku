package dev.mitchdunn.sudoku.viewmodel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.content.res.Resources
import android.util.DisplayMetrics
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import dev.mitchdunn.R
import dev.mitchdunn.sudoku.model.Board
import dev.mitchdunn.sudoku.model.Generator
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class SudokuBoardViewModelTest {

    // This rule swaps the background executor used by the
    // Architecture Components (in our case LiveData) with
    // a different one which executes each task synchronously.
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private lateinit var mockApp: Application

    @Before
    fun setupMocks() {
        val mockPref = mock<SharedPreferences> {
            on {getString("show_errors", "Show no errors")} doReturn "Do not insert"
        }
        val mockDisplayMetrics = mock<DisplayMetrics>()
        val mockResources = mock<Resources> {
            on {displayMetrics} doReturn mockDisplayMetrics
            on {getString(R.string.show_no_error)} doReturn "Show no errors"
            on {getString(R.string.do_not_insert)} doReturn "Do not insert"
        }
        val mockContext = mock<Context> {
            on {getSharedPreferences(any(), any())} doReturn mockPref
            on {resources} doReturn mockResources
        }
        mockApp = mock {
            on {applicationContext} doReturn mockContext
        }
    }

    @Test
    fun testAddNumber() {
        val viewModel = SudokuBoardViewModel(mockApp)
        val board = Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834600")
        board.finalize(Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834629"))
        viewModel.board.value = board
        viewModel.solved.value = false

        viewModel.addNumber(2, 9, 8)
        assertFalse(viewModel.solved.value!!)
        assertEquals(2, viewModel.board.value!!.getNumber(9, 8))

        viewModel.addNumber(3, 9, 8)
        assertFalse(viewModel.solved.value!!)
        assertEquals(2, viewModel.board.value!!.getNumber(9, 8))

        viewModel.addNumber(8, 9, 9)
        assertFalse(viewModel.solved.value!!)
        assertEquals(0, viewModel.board.value!!.getNumber(9, 9))

        viewModel.addNumber(9, 9, 9)
        assertTrue(viewModel.solved.value!!)
        assertEquals(9, viewModel.board.value!!.getNumber(9, 9))
    }

    @Test
    fun testClearNumber() {
        val viewModel = SudokuBoardViewModel(mockApp)
        viewModel.board.value = Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834600")
        assertEquals(0, viewModel.board.value!!.getNumber(9, 8))
        viewModel.clearNumber(9, 8)
        assertEquals(0, viewModel.board.value!!.getNumber(9, 8))
        assertEquals(6, viewModel.board.value!!.getNumber(9, 7))
        viewModel.clearNumber(9, 7)
        assertEquals(0, viewModel.board.value!!.getNumber(9, 7))
    }

    @Test
    fun testCreateNewGame() {
        val viewModel = SudokuBoardViewModel(mockApp)
        val board = Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834620")
        board.finalize(Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834629"))
        viewModel.board.value = board
        viewModel.solved.value = false
        assertFalse(viewModel.solved.value!!)
        viewModel.addNumber(9, 9, 9)
        assertTrue(viewModel.solved.value!!)
        val oldBoard = viewModel.board.value
        viewModel.createNewGame(Generator.generate())
        assertFalse(viewModel.solved.value!!)
        assertNotEquals(oldBoard, viewModel.board.value)
    }

    @Test
    fun testNotes() {
        val viewModel = SudokuBoardViewModel(mockApp)
        assertFalse(viewModel.getNote(1, 1, 1))
        assertFalse(viewModel.getNote(2, 1, 1))
        assertFalse(viewModel.getNote(3, 1, 1))
        assertFalse(viewModel.getNote(4, 1, 1))
        assertFalse(viewModel.getNote(5, 1, 1))
        assertFalse(viewModel.getNote(6, 1, 1))
        assertFalse(viewModel.getNote(7, 1, 1))
        assertFalse(viewModel.getNote(8, 1, 1))
        assertFalse(viewModel.getNote(9, 1, 1))
        assertFalse(viewModel.getNote(1, 2, 1))
        viewModel.addNote(1, 1, 1)
        viewModel.addNote(4, 1, 1)
        viewModel.addNote(7, 1, 1)
        viewModel.addNote(1, 2, 1)
        assertTrue(viewModel.getNote(1, 1, 1))
        assertFalse(viewModel.getNote(2, 1, 1))
        assertFalse(viewModel.getNote(3, 1, 1))
        assertTrue(viewModel.getNote(4, 1, 1))
        assertFalse(viewModel.getNote(5, 1, 1))
        assertFalse(viewModel.getNote(6, 1, 1))
        assertTrue(viewModel.getNote(7, 1, 1))
        assertFalse(viewModel.getNote(8, 1, 1))
        assertFalse(viewModel.getNote(9, 1, 1))
        assertTrue(viewModel.getNote(1, 2, 1))
        viewModel.clearNote(1, 1, 1)
        assertFalse(viewModel.getNote(1, 1, 1))
        assertFalse(viewModel.getNote(2, 1, 1))
        assertFalse(viewModel.getNote(3, 1, 1))
        assertTrue(viewModel.getNote(4, 1, 1))
        assertFalse(viewModel.getNote(5, 1, 1))
        assertFalse(viewModel.getNote(6, 1, 1))
        assertTrue(viewModel.getNote(7, 1, 1))
        assertFalse(viewModel.getNote(8, 1, 1))
        assertFalse(viewModel.getNote(9, 1, 1))
        assertTrue(viewModel.getNote(1, 2, 1))
        viewModel.clearAllNotes(1, 1)
        assertFalse(viewModel.getNote(1, 1, 1))
        assertFalse(viewModel.getNote(2, 1, 1))
        assertFalse(viewModel.getNote(3, 1, 1))
        assertFalse(viewModel.getNote(4, 1, 1))
        assertFalse(viewModel.getNote(5, 1, 1))
        assertFalse(viewModel.getNote(6, 1, 1))
        assertFalse(viewModel.getNote(7, 1, 1))
        assertFalse(viewModel.getNote(8, 1, 1))
        assertFalse(viewModel.getNote(9, 1, 1))
        assertTrue(viewModel.getNote(1, 2, 1))
    }

    @Test
    fun testGetHint() {
        val viewModel = SudokuBoardViewModel(mockApp)
        val board = Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834620")
        board.finalize(Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834629"))
        viewModel.board.value = board
        viewModel.solved.value = false

        viewModel.getHint(1, 1)
        assertEquals(8, viewModel.board.value!!.getNumber(1, 1))
        assertFalse(viewModel.solved.value!!)

        viewModel.getHint(9, 8)
        assertEquals(2, viewModel.board.value!!.getNumber(9, 8))
        assertFalse(viewModel.solved.value!!)

        viewModel.getHint(9, 9)
        assertEquals(9, viewModel.board.value!!.getNumber(9, 9))
        assertTrue(viewModel.solved.value!!)
    }
}