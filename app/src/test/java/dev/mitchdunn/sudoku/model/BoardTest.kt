package dev.mitchdunn.sudoku.model

import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Assert.assertNotEquals
import org.junit.Assert.assertTrue
import org.junit.Test

class BoardTest {

    private val fullBoard = Board("825471396" +
            "194326578" +
            "376985241" +
            "519743862" +
            "632598417" +
            "487612935" +
            "263159784" +
            "948267153" +
            "751834629")

    @Test
    fun constructor_copy() {
        val board1 = Board()
        board1.addNumber(1, 1, 1, true)
        board1.addNumber(2, 2, 2, true)
        val board2 = Board(board1)
        assertEquals(board1, board2)
        val board3 = Board()
        assertNotEquals(board1, board3)
    }

    @Test(expected = IllegalArgumentException::class)
    fun constructor_invalidLength() {
        Board("123456789")
    }

    @Test(expected = IllegalArgumentException::class)
    fun constructor_invalidCharacter() {
        Board("12345678912345678912345678912345A789123456789123456789123456789123456789123456789")
    }

    @Test
    fun constructor() {
        val board = Board("123456789123456789123456789123456789123456789123456789123456789123456789123456789")
        assertEquals(0, board.numberAmountRemaining(0))
        assertEquals(0, board.numberAmountRemaining(1))
        assertEquals(0, board.numberAmountRemaining(2))
        assertEquals(0, board.numberAmountRemaining(3))
        assertEquals(0, board.numberAmountRemaining(4))
        assertEquals(0, board.numberAmountRemaining(5))
        assertEquals(0, board.numberAmountRemaining(6))
        assertEquals(0, board.numberAmountRemaining(7))
        assertEquals(0, board.numberAmountRemaining(8))
        assertEquals(0, board.numberAmountRemaining(9))
    }

    @Test(expected = ArrayIndexOutOfBoundsException::class)
    fun getNumber_invalid() {
        val number = Board("123456789123456789123456789123456789123456789123456789123456789123456789123456789").getNumber(9, 10)
    }

    @Test
    fun finalize() {
        val board = Board("123456789000000000000000000000000000000000000000000000000000000000000000000000000")
        assertTrue(board.isCellEditable(1, 1))
        assertTrue(board.isCellEditable(2, 2))
        assertTrue(board.isCellEditable(3, 3))
        assertTrue(board.isCellEditable(1, 9))
        assertEquals(9, board.getNumber(1, 9))
        board.clearNumber(1,9)
        assertEquals(0, board.getNumber(1, 9))
        assertFalse(board.isNumberCorrect(1, 1))
        assertEquals(Difficulty.VERY_EASY, board.difficulty)
        board.finalize(Board("123456789000000000000000000200000000000000000000000000000000000000000000000000000"))
        assertEquals(Difficulty.HARD, board.difficulty)
        assertTrue(board.isNumberCorrect(1, 1))
        assertFalse(board.isCellEditable(1, 1))
        assertTrue(board.isCellEditable(2, 2))
        assertTrue(board.isCellEditable(3, 3))
        assertFalse(board.isCellEditable(1, 8))
        assertEquals(8, board.getNumber(1, 8))
        board.clearNumber(1,8)
        assertEquals(8, board.getNumber(1, 8))
        assertEquals(0, board.getNumber(4, 1))
        board.addNumber(2, 4, 1, false)
        assertEquals(2, board.getNumber(4, 1))
        board.clearNumber(4, 1)
        assertEquals(0, board.getNumber(4, 1))
    }

    @Test
    fun resetBoard() {
        val board = Board("123456789000000000000000000000000000000000000000000000000000000000000000000000000")
        board.finalize(Board("123456789987321654000000000000000000000000000000000000000000000000000000000000000"))
        board.addNumber(9, 2, 1, false)
        board.addNumber(8, 2, 2, false)
        board.addNumber(7, 2, 3, false)
        board.addNumber(3, 2, 4, false)
        board.addNumber(2, 2, 5, false)
        board.addNumber(1, 2, 6, false)
        board.addNumber(6, 2, 7, false)
        board.addNumber(5, 2, 8, false)
        board.addNumber(4, 2, 9, false)
        assertEquals(1, board.getNumber(1, 1))
        assertEquals(2, board.getNumber(1, 2))
        assertEquals(3, board.getNumber(1, 3))
        assertEquals(4, board.getNumber(1, 4))
        assertEquals(5, board.getNumber(1, 5))
        assertEquals(6, board.getNumber(1, 6))
        assertEquals(7, board.getNumber(1, 7))
        assertEquals(8, board.getNumber(1, 8))
        assertEquals(9, board.getNumber(1, 9))
        assertEquals(9, board.getNumber(2, 1))
        assertEquals(8, board.getNumber(2, 2))
        assertEquals(7, board.getNumber(2, 3))
        assertEquals(3, board.getNumber(2, 4))
        assertEquals(2, board.getNumber(2, 5))
        assertEquals(1, board.getNumber(2, 6))
        assertEquals(6, board.getNumber(2, 7))
        assertEquals(5, board.getNumber(2, 8))
        assertEquals(4, board.getNumber(2, 9))
        assertEquals(7, board.numberAmountRemaining(1))
        assertEquals(7, board.numberAmountRemaining(2))
        assertEquals(7, board.numberAmountRemaining(3))
        assertEquals(7, board.numberAmountRemaining(4))
        assertEquals(7, board.numberAmountRemaining(5))
        assertEquals(7, board.numberAmountRemaining(6))
        assertEquals(7, board.numberAmountRemaining(7))
        assertEquals(7, board.numberAmountRemaining(8))
        assertEquals(7, board.numberAmountRemaining(9))
        board.resetBoard()
        assertEquals(1, board.getNumber(1, 1))
        assertEquals(2, board.getNumber(1, 2))
        assertEquals(3, board.getNumber(1, 3))
        assertEquals(4, board.getNumber(1, 4))
        assertEquals(5, board.getNumber(1, 5))
        assertEquals(6, board.getNumber(1, 6))
        assertEquals(7, board.getNumber(1, 7))
        assertEquals(8, board.getNumber(1, 8))
        assertEquals(9, board.getNumber(1, 9))
        assertEquals(0, board.getNumber(2, 1))
        assertEquals(0, board.getNumber(2, 2))
        assertEquals(0, board.getNumber(2, 3))
        assertEquals(0, board.getNumber(2, 4))
        assertEquals(0, board.getNumber(2, 5))
        assertEquals(0, board.getNumber(2, 6))
        assertEquals(0, board.getNumber(2, 7))
        assertEquals(0, board.getNumber(2, 8))
        assertEquals(0, board.getNumber(2, 9))
        assertEquals(8, board.numberAmountRemaining(1))
        assertEquals(8, board.numberAmountRemaining(2))
        assertEquals(8, board.numberAmountRemaining(3))
        assertEquals(8, board.numberAmountRemaining(4))
        assertEquals(8, board.numberAmountRemaining(5))
        assertEquals(8, board.numberAmountRemaining(6))
        assertEquals(8, board.numberAmountRemaining(7))
        assertEquals(8, board.numberAmountRemaining(8))
        assertEquals(8, board.numberAmountRemaining(9))
    }

    @Test
    fun getNumber() {
        val board = Board("123456789123456789123456789123456789123456789123456789123456789123456789123456789")
        assertEquals(1, board.getNumber(1, 1))
        assertEquals(2, board.getNumber(1, 2))
        assertEquals(1, board.getNumber(4, 1))
        assertEquals(9, board.getNumber(9, 9))
    }

    @Test
    fun isCellEditable() {
        val board = Board("123456789000000000000000000000000000000000000000000000000000000000000000000000000")
        assertTrue(board.isCellEditable(1, 1))
        assertTrue(board.isCellEditable(2, 2))
        assertTrue(board.isCellEditable(3, 3))
        board.finalize(board)
        assertFalse(board.isCellEditable(1, 1))
        assertTrue(board.isCellEditable(2, 2))
        assertTrue(board.isCellEditable(3, 3))
    }

    @Test
    fun isNumberCorrect() {
        val board = Board("800001397" +
                "194006570" +
                "006985241" +
                "500040060" +
                "632598417" +
                "487612900" +
                "063109780" +
                "008267150" +
                "001834620")
        val solution = fullBoard
        board.finalize(solution)
        assertTrue(board.isNumberCorrect(1, 1))
        assertFalse(board.isNumberCorrect(1, 2))
        assertFalse(board.isNumberCorrect(1, 9))
        assertTrue(board.isNumberCorrect(5, 2))
        assertFalse(board.isNumberCorrect(7, 5))
        assertFalse(board.isNumberCorrect(9, 9))
    }

    @Test
    fun canNumberGoInPlace() {
        val board = Board("100000000" +
                "200000000" +
                "030000000" +
                "004000000" +
                "000000000" +
                "000000000" +
                "000000000" +
                "000000000" +
                "000000000")
        assertFalse(board.canNumberGoInPlace(1, 1, 1))
        assertFalse(board.canNumberGoInPlace(2, 1, 1))
        assertFalse(board.canNumberGoInPlace(2, 2, 1))
        assertFalse(board.canNumberGoInPlace(1, 1, 2))
        assertFalse(board.canNumberGoInPlace(2, 2, 2))
        assertFalse(board.canNumberGoInPlace(3, 3, 3))
        assertFalse(board.canNumberGoInPlace(1, 5, 1))
        assertFalse(board.canNumberGoInPlace(2, 6, 1))
        assertFalse(board.canNumberGoInPlace(3, 7, 2))
        assertFalse(board.canNumberGoInPlace(4, 1, 3))
        assertFalse(board.canNumberGoInPlace(1, 2, 2))
        assertFalse(board.canNumberGoInPlace(2, 1, 2))
        assertFalse(board.canNumberGoInPlace(3, 1, 3))
        assertFalse(board.canNumberGoInPlace(4, 5, 1))
        assertTrue(board.canNumberGoInPlace(9, 1, 1))
        assertTrue(board.canNumberGoInPlace(1, 2, 4))
        assertTrue(board.canNumberGoInPlace(2, 9, 2))
        assertTrue(board.canNumberGoInPlace(3, 9, 9))
        assertTrue(board.canNumberGoInPlace(4, 5, 4))
        assertTrue(board.canNumberGoInPlace(5, 1, 2))
        assertTrue(board.canNumberGoInPlace(6, 5, 5))
        assertTrue(board.canNumberGoInPlace(7, 8, 1))
        assertTrue(board.canNumberGoInPlace(8, 1, 8))
        assertTrue(board.canNumberGoInPlace(9, 9, 8))
    }

    @Test
    fun addNumber() {
        val board = Board("800000000" +
                "100000000" +
                "070000000" +
                "009000000" +
                "000000000" +
                "000000000" +
                "000000000" +
                "000000000" +
                "000000000")
        board.finalize(fullBoard)
        assertEquals(77, board.numberAmountRemaining(0))
        assertEquals(8, board.numberAmountRemaining(1))
        assertEquals(9, board.numberAmountRemaining(2))
        assertEquals(9, board.numberAmountRemaining(3))
        assertEquals(9, board.numberAmountRemaining(4))
        assertEquals(9, board.numberAmountRemaining(5))
        assertEquals(9, board.numberAmountRemaining(6))
        assertEquals(8, board.numberAmountRemaining(7))
        assertEquals(8, board.numberAmountRemaining(8))
        assertEquals(8, board.numberAmountRemaining(9))

        assertEquals(0, board.getNumber(2, 4))
        assertTrue(board.addNumber(3, 2, 4, false))
        assertEquals(76, board.numberAmountRemaining(0))
        assertEquals(8, board.numberAmountRemaining(3))
        assertEquals(3, board.getNumber(2, 4))

        assertEquals(0, board.getNumber(9, 2))
        assertTrue(board.addNumber(5, 9, 2, false))
        assertEquals(75, board.numberAmountRemaining(0))
        assertEquals(8, board.numberAmountRemaining(5))
        assertEquals(5, board.getNumber(9, 2))

        assertEquals(0, board.getNumber(9, 9))
        assertTrue(board.addNumber(9, 9, 9, false))
        assertEquals(74, board.numberAmountRemaining(0))
        assertEquals(7, board.numberAmountRemaining(9))
        assertEquals(9, board.getNumber(9, 9))

        assertEquals(0, board.getNumber(5, 4))
        assertTrue(board.addNumber(5, 5, 4, false))
        assertEquals(73, board.numberAmountRemaining(0))
        assertEquals(7, board.numberAmountRemaining(5))
        assertEquals(5, board.getNumber(5, 4))

        assertEquals(0, board.getNumber(1, 2))
        assertTrue(board.addNumber(2, 1, 2, false))
        assertEquals(72, board.numberAmountRemaining(0))
        assertEquals(8, board.numberAmountRemaining(2))
        assertEquals(2, board.getNumber(1, 2))

        assertEquals(0, board.getNumber(5, 5))
        assertTrue(board.addNumber(9, 5, 5, false))
        assertEquals(71, board.numberAmountRemaining(0))
        assertEquals(6, board.numberAmountRemaining(9))
        assertEquals(9, board.getNumber(5, 5))

        assertEquals(0, board.getNumber(8, 1))
        assertTrue(board.addNumber(9, 8, 1, false))
        assertEquals(70, board.numberAmountRemaining(0))
        assertEquals(5, board.numberAmountRemaining(9))
        assertEquals(9, board.getNumber(8, 1))

        assertEquals(0, board.getNumber(1, 8))
        assertTrue(board.addNumber(9, 1, 8, false))
        assertEquals(69, board.numberAmountRemaining(0))
        assertEquals(4, board.numberAmountRemaining(9))
        assertEquals(9, board.getNumber(1, 8))

        assertEquals(0, board.getNumber(9, 8))
        assertTrue(board.addNumber(2, 9, 8, false))
        assertEquals(68, board.numberAmountRemaining(0))
        assertEquals(7, board.numberAmountRemaining(2))
        assertEquals(2, board.getNumber(9, 8))

        assertEquals(0, board.getNumber(9, 7))
        assertFalse(board.addNumber(9, 9, 7, false))
        assertEquals(0, board.getNumber(9, 7))
        assertTrue(board.addNumber(9, 9, 7, true))
        assertEquals(9, board.getNumber(9, 7))
        assertEquals(67, board.numberAmountRemaining(0))
        assertEquals(3, board.numberAmountRemaining(9))

        assertEquals(0, board.getNumber(1, 3))
        assertFalse(board.addNumber(1, 1, 3, false))
        assertEquals(0, board.getNumber(1, 3))
        assertTrue(board.addNumber(1, 1, 3, true))
        assertEquals(1, board.getNumber(1, 3))
        assertEquals(66, board.numberAmountRemaining(0))
        assertEquals(7, board.numberAmountRemaining(1))
    }

    @Test
    fun notes() {
        val board = Board("100000000" +
                "200000000" +
                "300000000" +
                "400000000" +
                "500000000" +
                "600000000" +
                "700000000" +
                "800000000" +
                "900000000")
        for(i in 1..Board.BOARD_SIZE) {
            for (j in 1..Board.BOARD_SIZE) {
                for (k in 1..Board.BOARD_SIZE) {
                    assertFalse(board.getNote(i, j, k))
                }
            }
        }

        board.addNote(2, 2, 1)
        board.addNote(3, 2, 1)
        board.addNote(4, 2, 1)
        board.addNote(8, 2, 1)
        board.addNote(5, 3, 1)
        board.addNote(6, 3, 1)
        board.addNote(7, 3, 1)
        board.addNote(8, 3, 1)
        board.addNote(8, 9, 9)
        assertTrue(booleanArrayOf(false, true, true, true, false, false, false, true, false) contentEquals board.getNotes(2, 1))
        assertTrue(booleanArrayOf(false, false, false, false, true, true, true, true, false) contentEquals board.getNotes(3, 1))
        assertTrue(booleanArrayOf(false, false, false, false, false, false, false, true, false) contentEquals board.getNotes(9, 9))
        assertFalse(board.getNote(1, 2, 1))
        assertTrue(board.getNote(2, 2, 1))

        assertFalse(board.getNote(5, 4, 3))
        board.addNote(5, 4, 3)
        assertTrue(board.getNote(5, 4, 3))
        board.clearNote(5, 4, 3)
        assertFalse(board.getNote(5, 4, 3))

        assertTrue(board.getNote(8, 2, 1))
        assertTrue(board.getNote(8, 3, 1))
        assertTrue(board.getNote(8, 9, 9))
        board.clearNote(8)
        assertFalse(board.getNote(8, 2, 1))
        assertFalse(board.getNote(8, 3, 1))
        assertFalse(board.getNote(8, 9, 9))

        assertTrue(booleanArrayOf(false, true, true, true, false, false, false, false, false) contentEquals board.getNotes(2, 1))
        assertTrue(board.getNote(2, 2, 1))
        board.clearAllNotes(2, 1)
        assertTrue(booleanArrayOf(false, false, false, false, false, false, false, false, false) contentEquals board.getNotes(2, 1))
        assertFalse(board.getNote(2, 2, 1))
    }

    @Test
    fun getHint() {
        val board = Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834000")
        board.finalize(fullBoard)

        assertEquals(0, board.hints)
        assertEquals(8, board.getNumber(1, 1))
        board.getHint(1, 1)
        assertEquals(8, board.getNumber(1, 1))
        assertEquals(1, board.hints)

        assertEquals(9, board.getNumber(5, 5))
        board.getHint(5, 5)
        assertEquals(9, board.getNumber(5, 5))
        assertEquals(2, board.hints)

        assertEquals(0, board.getNumber(9, 7))
        board.getHint(9, 7)
        assertEquals(6, board.getNumber(9, 7))
        assertEquals(3, board.hints)

        assertEquals(0, board.getNumber(9, 8))
        board.getHint(9, 8)
        assertEquals(2, board.getNumber(9, 8))
        assertEquals(4, board.hints)

        assertEquals(0, board.getNumber(9, 9))
        board.getHint(9, 9)
        assertEquals(9, board.getNumber(9, 9))
        assertEquals(5, board.hints)
    }

    @Test
    fun clearNumber() {
        val board = Board("100000000" +
                "200000000" +
                "300000000" +
                "400000000" +
                "500000000" +
                "600000000" +
                "700000000" +
                "800000000" +
                "910000000")
        assertEquals(71, board.numberAmountRemaining(0))
        assertEquals(7, board.numberAmountRemaining(1))
        assertEquals(8, board.numberAmountRemaining(2))
        assertEquals(8, board.numberAmountRemaining(3))
        assertEquals(8, board.numberAmountRemaining(4))
        assertEquals(8, board.numberAmountRemaining(5))
        assertEquals(8, board.numberAmountRemaining(6))
        assertEquals(8, board.numberAmountRemaining(7))
        assertEquals(8, board.numberAmountRemaining(8))
        assertEquals(8, board.numberAmountRemaining(9))

        assertEquals(1, board.getNumber(1, 1))
        board.clearNumber(1, 1)
        assertEquals(0, board.getNumber(1, 1))
        assertEquals(72, board.numberAmountRemaining(0))
        assertEquals(8, board.numberAmountRemaining(1))

        assertEquals(2, board.getNumber(2, 1))
        board.clearNumber(2, 1)
        assertEquals(0, board.getNumber(2, 1))
        assertEquals(73, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(2))

        assertEquals(3, board.getNumber(3, 1))
        board.clearNumber(3, 1)
        assertEquals(0, board.getNumber(3, 1))
        assertEquals(74, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(3))

        assertEquals(4, board.getNumber(4, 1))
        board.clearNumber(4, 1)
        assertEquals(0, board.getNumber(4, 1))
        assertEquals(75, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(4))

        assertEquals(5, board.getNumber(5, 1))
        board.clearNumber(5, 1)
        assertEquals(0, board.getNumber(5, 1))
        assertEquals(76, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(5))

        assertEquals(6, board.getNumber(6, 1))
        board.clearNumber(6, 1)
        assertEquals(0, board.getNumber(6, 1))
        assertEquals(77, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(6))

        assertEquals(7, board.getNumber(7, 1))
        board.clearNumber(7, 1)
        assertEquals(0, board.getNumber(7, 1))
        assertEquals(78, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(7))

        assertEquals(8, board.getNumber(8, 1))
        board.clearNumber(8, 1)
        assertEquals(0, board.getNumber(8, 1))
        assertEquals(79, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(8))

        assertEquals(9, board.getNumber(9, 1))
        board.clearNumber(9, 1)
        assertEquals(0, board.getNumber(9, 1))
        assertEquals(80, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(9))

        assertEquals(1, board.getNumber(9, 2))
        board.clearNumber(9, 2)
        assertEquals(0, board.getNumber(9, 2))
        assertEquals(81, board.numberAmountRemaining(0))
        assertEquals(9, board.numberAmountRemaining(1))
    }

    @Test
    fun isValidRow() {
        val board = Board("123456789" +
                "987654321" +
                "456123987" +
                "674031895" +
                "000000000" +
                "112233445" +
                "000000000" +
                "000000000" +
                "000000000")
        assertTrue(board.isValidRow(1))
        assertTrue(board.isValidRow(2))
        assertTrue(board.isValidRow(3))
        assertFalse(board.isValidRow(4))
        assertFalse(board.isValidRow(5))
        assertFalse(board.isValidRow(6))
    }

    @Test
    fun isValidCol() {
        val board = Board("194156789" +
                "285254321" +
                "376323987" +
                "461431895" +
                "552500000" +
                "643633445" +
                "739700000" +
                "828800000" +
                "917000000")
        assertTrue(board.isValidCol(1))
        assertTrue(board.isValidCol(2))
        assertTrue(board.isValidCol(3))
        assertFalse(board.isValidCol(4))
        assertFalse(board.isValidCol(5))
        assertFalse(board.isValidCol(6))
    }

    @Test
    fun isValidGrid() {
        val board = Board("123456789" +
                "456789123" +
                "789123456" +
                "123987895" +
                "456654000" +
                "780633445" +
                "000000000" +
                "000000000" +
                "000000000")
        assertTrue(board.isValidGrid(1))
        assertTrue(board.isValidGrid(2))
        assertTrue(board.isValidGrid(3))
        assertFalse(board.isValidGrid(4))
        assertFalse(board.isValidGrid(5))
        assertFalse(board.isValidGrid(6))
        assertFalse(board.isValidGrid(7))
        assertFalse(board.isValidGrid(8))
        assertFalse(board.isValidGrid(9))
    }

    @Test
    fun isValidPuzzle() {
        val board = Board("825471396" +
                "194326578" +
                "376985241" +
                "519743862" +
                "632598417" +
                "487612935" +
                "263159784" +
                "948267153" +
                "751834620")
        board.finalize(fullBoard)
        assertFalse(board.isValidPuzzle)
        board.addNumber(9, 9, 9, false)
        assertTrue(board.isValidPuzzle)
        board.clearNumber(9, 9)
        board.addNumber(8, 9, 9, true)
        assertFalse(board.isValidPuzzle)
    }

    @Test
    fun prettyPrint() {
        assertEquals("+---+---+---+\n" +
                "|825|471|396|\n" +
                "|194|326|578|\n" +
                "|376|985|241|\n" +
                "+---+---+---+\n" +
                "|519|743|862|\n" +
                "|632|598|417|\n" +
                "|487|612|935|\n" +
                "+---+---+---+\n" +
                "|263|159|784|\n" +
                "|948|267|153|\n" +
                "|751|834|629|\n" +
                "+---+---+---+", fullBoard.prettyPrint())
    }

    @Test
    fun boardToString() {
        var string = "614798532829135746357426981983261475462957318571384629238579164196843257745612893"
        assertEquals(string, Board(string).toString())

        string = "005000000703516090004000003050890301847030569391000042000070426076002035029003180"
        assertEquals(string, Board(string).toString())

        string = "718392456926145873543678129135964782697281534482753691861539247374826915259417368"
        assertEquals(string, Board(string).toString())
    }
}