package dev.mitchdunn.sudoku.model

import dev.mitchdunn.sudoku.model.Generator.generate
import org.junit.Assert.assertEquals
import org.junit.Assert.assertFalse
import org.junit.Test

class GeneratorTest {

    @Test
    fun testGenerate() {
        var board: Board
        var board2: Board
        val solutions = HashSet<Board>()
        for (i in 1..500) {
            board = generate()
            board2 = generate()
            assertFalse(board == board2)
            Solver.solve(board, solutions)
            assertEquals(1, solutions.size)
            Solver.solve(board2, solutions)
            assertEquals(1, solutions.size)
        }
    }

    @Test
    fun testGenerate_difficulty() {
        assertEquals(Difficulty.VERY_EASY, generate(Difficulty.VERY_EASY).difficulty)
        assertEquals(Difficulty.EASY, generate(Difficulty.EASY).difficulty)
        assertEquals(Difficulty.MEDIUM, generate(Difficulty.MEDIUM).difficulty)
        assertEquals(Difficulty.HARD, generate(Difficulty.HARD).difficulty)
    }
}