package dev.mitchdunn.sudoku.model

import org.junit.Assert.assertEquals
import org.junit.Test

class DifficultyTest {

    @Test
    fun getDifficulty() {
        var board = Board("825471396" +
                "194326578" +
                "370985041" +
                "519743062" +
                "630598017" +
                "487612935" +
                "263150784" +
                "948267153" +
                "751834029")
        assertEquals(Difficulty.VERY_EASY, Difficulty.getDifficulty(board))

        board = Board("820070306" +
                "190320508" +
                "370900041" +
                "500743062" +
                "630500017" +
                "400610035" +
                "263150784" +
                "948267153" +
                "751834029")
        assertEquals(Difficulty.EASY, Difficulty.getDifficulty(board))

        board = Board("820071096" +
                "190000508" +
                "300985041" +
                "519743002" +
                "630008017" +
                "487612005" +
                "203000784" +
                "900067103" +
                "700004009")
        assertEquals(Difficulty.MEDIUM, Difficulty.getDifficulty(board))

        board = Board("800070090" +
                "100000508" +
                "300000041" +
                "509000002" +
                "630008010" +
                "400000005" +
                "203000784" +
                "940060103" +
                "700004009")
        assertEquals(Difficulty.HARD, Difficulty.getDifficulty(board))
    }
}