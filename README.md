# Introduction

This is a simple Android application that allows a user to solve Sudoku puzzles.  It uses the Model-View-ViewModel
architecture.  As of v1.0.0, a user receives a puzzle based on the selected difficulty that they can then try to solve.
The puzzle is created by randomly backtracking a blank board until the board is filled in, then
numbers are randomly removed from the board until no more numbers can be removed without making the
puzzle non-unique (having more than 1 solution).  The difficulty is currently based on the number of
empty cells for the created puzzle (the more empty cells there are, the harder the puzzle, while not
ideal this works for now).  Users can also change the display settings so that instead of 1-9 they can
solve a puzzle using letters or even colors (colors is currently experimental and may be a bit buggy).
The Board object which stores the model always uses 0-9 (0 is used for blank spaces), the display settings
only affect the view.


## Signing

In order to sign the release AAB, a secrets.properties file must be included at the root level; this properties file 
needs 'storePassword', 'keyAlias', and 'keyPassword' entries.  The app/build.gradle file looks for the properties file
and these keys and uses them as part of the signing config.  By default the signing config is setup to use the keystore
found at the root level.  If build.gradle cannot find the secrets.properties file then it is assumed we are in a CI
environment, at which point build.gradle will attempt to get the keys from the system environment.


## Publishing

This repository is setup to use Gitlab CI and fastlane in order to publish AABs to Google Play Store.   When promoting
the .gitlab-ci.yml file will grab the secret json file via environment variable for fastlane to use; once the promotion
is done this file is deleted.  Additionally the versionCode used is based on the CI build number which means it will
automatically increment each time we run a build (in non-CI environments the versionCode defaults to 1).