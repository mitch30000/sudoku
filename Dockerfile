# This Dockerfile creates a static build image for CI

FROM openjdk:8-jdk

# Just matched `app/build.gradle`
ENV ANDROID_COMPILE_SDK "29"
# Just matched `app/build.gradle`
ENV ANDROID_BUILD_TOOLS "29.0.3"
# Version from https://developer.android.com/studio/releases/sdk-tools
ENV ANDROID_SDK_TOOLS "4333796"

ENV ANDROID_HOME /android-sdk-linux
ENV PATH="${PATH}:${ANDROID_HOME}/platform-tools/:${ANDROID_HOME}/tools/bin/"

# install OS packages
RUN apt-get --quiet update --yes
RUN apt-get --quiet install --yes wget apt-utils tar unzip lib32stdc++6 lib32z1 build-essential ruby ruby-dev
# We use this for xxd hex->binary
RUN apt-get --quiet install --yes vim-common
# install Android SDK
RUN wget --quiet --output-document=android-sdk.zip https://dl.google.com/android/repository/sdk-tools-linux-${ANDROID_SDK_TOOLS}.zip
RUN unzip -qq android-sdk.zip -d ${ANDROID_HOME}
RUN rm -f android-sdk.zip
#RUN mkdir -p ~/.android
#RUN echo "count=0" > ~/.android/repositories.cfg
RUN echo y | sdkmanager --update
RUN echo y | sdkmanager 'platforms;android-'${ANDROID_COMPILE_SDK}
RUN echo y | sdkmanager 'platform-tools'
RUN echo y | sdkmanager 'build-tools;'${ANDROID_BUILD_TOOLS}
RUN echo y | sdkmanager 'extras;android;m2repository'
RUN echo y | sdkmanager 'extras;google;google_play_services'
RUN echo y | sdkmanager 'extras;google;m2repository'
RUN echo y | sdkmanager --licenses
# install FastLane
COPY Gemfile.lock .
COPY Gemfile .
RUN gem install bundle
RUN bundle update --bundler
RUN bundle install
RUN bundle update --all
